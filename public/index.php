<?php
define('BASE_DIR', __DIR__ . '/..');
define('APP_DIR', BASE_DIR . '/app');
define('VIEWS_DIR', BASE_DIR . '/resources/views');
define('CACHE_DIR', BASE_DIR . '/storage/cache');


require __DIR__.'/../vendor/autoload.php';
require APP_DIR . '/../bootstrap/app.php';