<?php

namespace App\Controllers;

use eftec\bladeone\BladeOne;

class Controller
{
    /**
     * Подключаю Blade, т.к. привычно в нем работать со вьюшками.
     * @param string $name
     * @param array  $vars
     *
     * @return string
     * @throws \Exception
     */
    public function view(string $name, array $vars = [])
    {
        $blade = new BladeOne(VIEWS_DIR, CACHE_DIR . '/views',BladeOne::MODE_DEBUG); // MODE_DEBUG allows to pinpoint troubles.
        return $blade->run($name, $vars);
    }
}