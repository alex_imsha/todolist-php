<?php

namespace App\Controllers;

use App\Models\Task;
use Faker;

class TaskListController extends Controller
{
    public function __invoke()
    {

        $faker   =  Faker\Factory::create('ru_RU');
        $tasks = [];
        $counter = 1;
        while ($counter < 12) {
            $task = new Task();
            $task->id =  $counter;
            $task->name =  $faker->text(100);
            $task->email =  $faker->email;
            $task->content =  $faker->text;
            $tasks[] = $task;
            $counter++;
        }
        return $this->view('task.list', compact('tasks'));
    }
}