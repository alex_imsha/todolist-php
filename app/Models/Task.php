<?php

namespace App\Models;

/**
 * Class Task
 *
 * @package App\Models
 */
class Task
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $content;

    /**
     * @return int
     */
    public function save()
    {
        return $this->id;
    }
}