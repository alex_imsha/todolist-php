<?php

namespace App\Models;

/**
 * Class User
 *
 * @package App\Models
 */
class User
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $role;

    /**
     * @var string
     */
    public $password;

    /**
     * @return int
     */
    public function save()
    {
        return $this->id;
    }

}