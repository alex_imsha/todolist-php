<?php
/**
 * @see \App\Controllers\TaskListController
 *
 * @var array $tasks
 */
?>
@extends('layouts.app')

@section('title', 'Список задач')

@section('content')

    <div class="row">
        <div class="col">
            <ul>
                @foreach ($tasks as $task)
                    <li>
                        {{$task->name}}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

@endsection